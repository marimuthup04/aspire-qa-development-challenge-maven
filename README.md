# aspire-qa-development-challenge-maven

Prerequisites:
Java - jdk-16.0.2
Intellij IDE
Plugins-Maven
Plugins-TestNG

Chrome Browser - Version 106.0.5249.119

Please use pom.xml to download necessary jar files

TestNG Run Configuration File:
Please use .idea\runConfigurations\aspire_testng.xml to run the test cases.