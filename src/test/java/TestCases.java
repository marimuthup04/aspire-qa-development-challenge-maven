import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;
import Utilities.WEB_Methods;

import java.io.IOException;

import static Utilities.WEB_Methods.driver;


public class TestCases {
    public static final String userName = "input#login.form-control";
    public static final String passWord = "input#password.form-control";
    public static final String loginButton = "button.btn.btn-primary.btn-block";
    public static final String inventory = "//div[@class='o_home_menu_scrollable']/div/a[2]/..//div[contains(text(),'Inventory')]";
    public static final String productsMainMenu = "//nav[@class='o_main_navbar']/div[1]/div[2]/button";
    public static final String productsSubMenu = "//nav[@class='o_main_navbar']/div[1]/div[2]/div/a";
    public static final String createButton = "//div[@class='o_control_panel']/div[2]/div/div/div/..//button[contains(text(),'Create')]";
    public static final String productName_TextField = "input#o_field_input_12.o_field_char.o_field_widget.o_quick_editable.o_input.o_field_translate.o_required_modifier.o_text_overflow";
    public static final String saveButton = "//div[@class='o_cp_buttons']/div/div[2]/button[1]";
    public static final String actionDropdown = "div.btn-group.dropdown";
    public static final String updateQuantityTab = "//div[@class='o_content']/div/div/div/div/button[2]/..//span[contains(text(),'Update Quantity')]";
    public static final String createButton2 = "button.btn.btn-primary.o_list_button_add";
    public static final String countedQuantity = "//div[@class='table-responsive']/table/tbody/tr/td[6]/input";
    public static final String saveButton_UpdateQuantity = "//div[@class='o_control_panel']/div[2]/div/div/div/..//button[@class='btn btn-primary o_list_button_save']";
    public static final String applicationIcon = "//nav[@class='o_main_navbar']/a";
    public static final String manufacturing = "//div[@class='o_home_menu_scrollable']/div/a[2]/..//div[contains(text(),'Manufacturing')]";
    public static final String productDropdown = "//div[@name='product_id']/div/div/input";
    public static final String createButton3 = "//div[@class='modal-content']/footer/button[1]";
    public static final String confirmButton = "//div[@class='o_form_statusbar']/div/..//button[@name='action_confirm']";
    public static final String markAsDoneButton = "(//div[@class='o_form_statusbar']/div/button[4])[1]";
    public static final String okButton = "//div[@class='modal-content']/footer/button[1]";
    public static final String applyButton = "//div[@class='modal-content']/footer/div/footer/button[1]";
    public static final String confirmedMessage = "(//div[@class='o_Message_core flex-grow-1'])[1]/div[2]/ul/li[1]/div/div[2]";
    public static final String doneMessage = "(//div[@class='o_Message_core flex-grow-1'])[1]/div[2]/ul/li[1]/div/div[4]";
    public static final String userButton = "div.o-dropdown.dropdown.o-dropdown--no-caret.o_user_menu>button";
    public static final String logoutButton = "//div[@class='o-dropdown dropdown o-dropdown--no-caret o_user_menu show']/div/a[3]";

    @Test(description = "Login to Web Application")
    public void test1() throws IOException, InterruptedException {
        driver.get("https://codechallenge.odoo.com/web/login");
        WEB_Methods.wait = new WebDriverWait(driver, 150);
        WEB_Methods.WEB_findElement("CSS STRING", userName).sendKeys("user@codechallenge.app");
        WEB_Methods.WEB_findElement("CSS STRING", passWord).sendKeys("@sp1r3app");
        WEB_Methods.WEB_click(WEB_Methods.WEB_findElement("CSS STRING", loginButton));
        System.out.println("Login to Web Application is Successful");
    }

    @Test(description = "Navigate to `Inventory` feature")
    public void test2() throws IOException, InterruptedException {
        WEB_Methods.WEB_click(WEB_Methods.WEB_findElement("STRING", inventory));
        WEB_Methods.wait = new WebDriverWait(driver, 60);
        System.out.println("Navigated to `Inventory` is Successful");
    }

    @Test(description = "Click on Products Menu and Create a New Product")
    public void test3() throws IOException, InterruptedException {
        WEB_Methods.WEB_click(WEB_Methods.WEB_findElement("STRING", productsMainMenu));
        WEB_Methods.WEB_click(WEB_Methods.WEB_findElement("STRING", productsSubMenu));
        WEB_Methods.wait = new WebDriverWait(driver, 120);
        WEB_Methods.WEB_click(WEB_Methods.WEB_findElement("STRING", createButton));
        WEB_Methods.wait = new WebDriverWait(driver, 60);
        WEB_Methods.WEB_findElement("CSS STRING", productName_TextField).sendKeys("20221017_01");
        WEB_Methods.WEB_click(WEB_Methods.WEB_findElement("STRING", saveButton));
        WEB_Methods.WEB_isDisplayed(driver, "CSS STRING", actionDropdown);
        System.out.println("Clicked on Products Menu and Created a New Product is Successful");

    }

    @Test(description = "Update the quantity of new product is more than 10")
    public void test4() throws IOException, InterruptedException {
        WEB_Methods.WEB_click(WEB_Methods.WEB_findElement("STRING", updateQuantityTab));
        Thread.sleep(6000);
        WEB_Methods.WEB_click(WEB_Methods.WEB_findElement("CSS STRING", createButton2));
        WEB_Methods.WEB_click((WEB_Methods.WEB_findElement("STRING", countedQuantity)));
        WEB_Methods.WEB_findElement("STRING", countedQuantity).clear();
        WEB_Methods.WEB_findElement("STRING", countedQuantity).sendKeys("15");
        WEB_Methods.WEB_click(WEB_Methods.WEB_findElement("STRING", saveButton_UpdateQuantity));
        System.out.println("Updated the quantity of new product is more than 10 is Successful");
    }

    @Test(description = "Click on Application Icon")
    public void test5() throws IOException, InterruptedException {
        WEB_Methods.WEB_click(WEB_Methods.WEB_findElement("STRING", applicationIcon));
        WEB_Methods.wait = new WebDriverWait(driver, 60);
        System.out.println("Clicked on Application Icon is Successful");
    }

    @Test(description = "Navigate to `Manufacturing` feature, then create a new Manufacturing Order item for the created Product on step #3")
    public void test6() throws IOException, InterruptedException {
        WEB_Methods.WEB_click(WEB_Methods.WEB_findElement("STRING", manufacturing));
        WEB_Methods.wait = new WebDriverWait(driver, 60);
        WEB_Methods.WEB_click(WEB_Methods.WEB_findElement("CSS STRING", createButton2));
        WEB_Methods.WEB_findElement("STRING", productDropdown).sendKeys("20221017_01");
//        WEB_Methods.WEB_findElement("STRING",productDropdown).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath("//html")).click();
        WEB_Methods.WEB_click(WEB_Methods.WEB_findElement("STRING", createButton3));
        System.out.println("Navigated to `Manufacturing` feature, then create a new Manufacturing Order item for the created Product on step #3 is Successful");
    }

    @Test(description = "Update the status of new Orders to “Done” successfully")
    public void test7() throws IOException, InterruptedException {
        WEB_Methods.WEB_click(WEB_Methods.WEB_findElement("STRING",confirmButton));
        WEB_Methods.wait = new WebDriverWait(driver, 60);
        WEB_Methods.WEB_click(WEB_Methods.WEB_findElement("STRING",markAsDoneButton));
//        WEB_Methods.WEB_Actionhover(markAsDoneButton);
//        WEB_Methods.WEB_clickJS(WEB_Methods.WEB_findElement("STRING",markAsDoneButton));
//        WEB_Methods.WEB_ActionClick(WEB_Methods.WEB_findElement("STRING",markAsDoneButton));
        WEB_Methods.wait = new WebDriverWait(driver, 60);
        WEB_Methods.WEB_click(WEB_Methods.WEB_findElement("STRING",okButton));
        WEB_Methods.WEB_click(WEB_Methods.WEB_findElement("STRING",applyButton));
        System.out.println("Updated the status of new Orders to “Done” is successful");
    }

    @Test(description = "Validate the new Manufacturing Order is created with corrected information")
    public void test8() throws IOException, InterruptedException {
        System.out.println("Validate the new Manufacturing Order is created with corrected information");
        Thread.sleep(6000);
        System.out.println("********** Message is " + WEB_Methods.WEB_findElement("STRING", confirmedMessage).getText() + " ****************");
        Assert.assertEquals("Confirmed Message is not Displayed", WEB_Methods.WEB_findElement("STRING", confirmedMessage).getText(), "Confirmed");
        System.out.println("********** Message is " + WEB_Methods.WEB_findElement("STRING", doneMessage).getText() + " *********************");
        Assert.assertEquals("Done Message is not Displayed", WEB_Methods.WEB_findElement("STRING", doneMessage).getText(), "Done");
    }

    @BeforeSuite(description = "Setup Driver")
    public void beforeSuite() throws IOException, InterruptedException {
        String currentDirectory=System.getProperty("user.dir");
        System.out.println("***** Current Project Directory is "+ currentDirectory + " *****");
        String driverPath=currentDirectory + "\\Drivers\\chromedriver.exe";
        System.setProperty("webdriver.chrome.driver", driverPath);
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @AfterSuite
    public void afterSuite() throws Exception {
        WEB_Methods.WEB_click(WEB_Methods.WEB_findElement("CSS STRING",userButton));
        WEB_Methods.WEB_click(WEB_Methods.WEB_findElement("STRING",logoutButton));
        WEB_Methods.Quit();
    }
}

