package Utilities;

import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.*;
import java.util.NoSuchElementException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

    public class WEB_Methods {
        private static final Logger logger = LoggerFactory.getLogger(WEB_Methods.class);
        public static WebDriver driver = null;
        public static WebDriverWait wait;
        public static WebElement element;


        public static WebDriver getDriver() {
            return driver;
        }

        public static boolean WEB_isDisplayed(WebDriver driver, String attributeType, String attribute) {
            WebElement element = null;
            try {
                element = WEB_findElement(attributeType, attribute);
                if (element.isDisplayed()) ;
                return true;
            } catch (Exception e) {
                // TODO: handle exception
            }
            return false;
        }

        public static WebElement WEB_findElement(String attributeType, String attribute)
                throws IOException, InterruptedException {
            // get property key value from properties file WebDriver driver,
            String strPropertyVal = "";

            if (attributeType.contains("STRING") == false) {
            }
            WebElement element = null;
            // if the locator value is not specified in the properties file

            if (strPropertyVal != null) {
                try {
                    wait = new WebDriverWait(driver,180);
                    switch (attributeType.toUpperCase()) {
                        case "ID":
                            element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(strPropertyVal.trim())));
                            driver.findElement(By.id(strPropertyVal.trim()));
                            break;
                        case "ID STRING":
                            element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(attribute.trim())));
                            break;
                        case "NAME":
                            element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(strPropertyVal.trim())));
                            break;
                        case "TAG NAME":
                            element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName(strPropertyVal.trim())));
                            break;
                        case "CLASS NAME":
                            element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(strPropertyVal.trim())));
                            break;
                        case "CSS":
                            element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(strPropertyVal.trim())));
                            break;
                        case "PARTIAL LINKTEXT":
                            element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.partialLinkText(strPropertyVal.trim())));
                            break;
                        case "LINKTEXT":
                            element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(strPropertyVal.trim())));
                            break;
                        case "STRING":
                            element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(attribute)));
                            break;
                        case "CSS STRING":
                            element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(attribute)));
                            break;
                        case "XPATH":
                            element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(strPropertyVal)));
                        default:
                            element = driver.findElement(By.xpath(strPropertyVal.trim()));
                    }
                } catch (NoSuchElementException e) {
                    logger.info("Unable to find element with " + attribute);
                }
            } else {
                logger.info("WEB findElement Locator: '" + attribute + "' is not found in properties file");
            }
            // highlighterMethod (driver, element);
            return element;
        }

        public static void WEB_click(WebElement element) {
            wait = new WebDriverWait(driver, 150);
//        element = wait.until(ExpectedConditions.elementToBeClickable(element));
            if (element != null) {
                WebDriver driver = getDriver();
                try {
                    element.click();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                logger.info("WEB_click-Unable to find the element");
            }
        }

        public static void WEB_clickJS(WebElement element) {
            wait = new WebDriverWait(driver, 158);
            element = wait.until(ExpectedConditions.elementToBeClickable(element));
            if (element != null) {
                WebDriver driver = getDriver();
                try {
                    JavascriptExecutor js = (JavascriptExecutor) driver;
                    js.executeScript("arguments[0].click();", element);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                logger.info("WEB click-Unable to find the element");
            }
        }

        public static void WEB_ActionClick(WebElement element) {
            wait = new WebDriverWait(driver, 150);
            element = wait.until(ExpectedConditions.elementToBeClickable(element));
            if (element != null) {
                WebDriver driver = getDriver();
                try {
                    Actions builder = new Actions(driver);
                    org.openqa.selenium.interactions.Action myAction = builder.click(element)
                            .release()
                            .build();

                    myAction.perform();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                logger.info("WEB click-Unable to find the element");
            }
        }

        public static void WEB_Actionhover(String button) {
            wait = new WebDriverWait(driver, 150);
            if (element != null) {
                WebDriver driver = getDriver();
                try {
                    Actions builder = new Actions(driver);
                    WebElement pointer = driver.findElement(By.xpath("//div[@class='o_form_statusbar']/div/..//button[@class='btn btn-primary']/span"));
                    builder.moveToElement(pointer);
                    builder.click().build().perform();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }


        public static void WEB_SendKeys(WebElement element, String Value) {
            wait = new WebDriverWait(driver, 150);
            element = wait.until(ExpectedConditions.visibilityOf(element));
            if (element != null) {
                try {
                    element.sendKeys(Value);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                logger.info(element + " is not visible in the webpage");
            }
        }

        public static void Quit() throws Exception {
            driver.quit();
        }

    }
